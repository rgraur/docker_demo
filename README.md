# Docker demo #

### Requirements ###
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)

### Install ###
* clone repo `git clone git@bitbucket.org:rgraur/docker_demo.git && cd docker_demo`
* update all submodules `git submodule update --init --recursive`
* initialize project `vagrant up`

### Run ###
* open in browser [http://localhost:8000](http://localhost:8000)
* database access: see [https://bitbucket.org/rgraur/docker_mysql](https://bitbucket.org/rgraur/docker_mysql/)
* after `vagrant reload` you have to start containers manually by running `/vagrant/startup.sh` or use [upstart](http://upstart.ubuntu.com/) or [systemd](http://en.wikipedia.org/wiki/Systemd)

### Structure ###
* [Debian 7.7 host VM](https://bitbucket.org/rgraur/vagrant/downloads/debian_7_7_0_amd64.box) (forwarded ports: 8000:80, 3306:3306; IP: 192.168.2.5)
* [Apache + PHP 5.5 Docker container](https://bitbucket.org/rgraur/docker_apache_php) (forwarded port: 80)
* [MySQL Docker container](https://bitbucket.org/rgraur/docker_mysql) (forwarded port: 3306)