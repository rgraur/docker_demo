#!/usr/bin/env bash

# install Docker
echo ">>> Installing Docker..."
apt-get install -qq curl
curl -sSL https://get.docker.com | sh
usermod -aG docker vagrant

# cleanup
docker rmi -f $(docker images -q)
apt-get purge -y --auto-remove curl

# MySQL container
echo ">>> Init MySQL Docker container..."
docker build -t demo/mysql /vagrant/docker/mysql

# Apache container
echo ">>> Init Apache Docker container..."
docker build -t demo/apache /vagrant/docker/apache
