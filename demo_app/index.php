<?php
/**
 * Docker multiple containers demo.
 *
 * Demonstrates connection to a mysql server running on other container.
 */

echo "<h3>Users</h3>";

// connect to database from another container
$conn = new mysqli('db', 'user', 'user', 'demo');
if ($conn->connect_error)
{
  die('Connection failed: ' . $conn->connect_error);
}

// create table and populate
$conn->query('DROP TABLE IF EXISTS `users`');
$conn->query(
  'CREATE TABLE `users` (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `name` varchar(20) NOT NULL,
     PRIMARY KEY (`id`)
  )'
);
$conn->query("INSERT INTO `users` VALUES (1, 'user1'),(2, 'user2')");

// select from created table and show results
if($result = $conn->query('SELECT * FROM `users`'))
{
  while($row = $result->fetch_object())
  {
    echo $row->id . ' - ' . $row->name . '<br />';
  }
}
else
{
  echo 'No entries found...';
}

$conn->close();

