#!/usr/bin/env bash

# start Docker containers
echo ">>> Starting Docker containers..."

# remove all containers first
docker rm -f $(docker ps -aq)

# mysql
docker run --name db -p 3306:3306 -v /data/mysql:/var/lib/mysql \
  -e MYSQL_USER="user" \
  -e MYSQL_PASS="user" \
  -e MYSQL_DB="demo" \
  -d demo/mysql

# apache
docker run --name web -p 80:80 --link db:db -v /vagrant/demo_app:/var/www -d demo/apache
